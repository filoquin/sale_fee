# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import time
from datetime import datetime


from openerp import tools
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.exceptions import UserError
import logging
_logger = logging.getLogger(__name__)


class pos_make_payment(osv.osv_memory):
    _inherit = 'pos.make.payment'

    def check(self, cr,uid, ids, context=None):
        context = context or {}
        order_obj = self.pool.get('pos.order')
        active_id = context and context.get('active_id', False)

        order = order_obj.browse(cr, uid, active_id, context=context)

        if not order.partner_id:
            raise osv.except_osv(_('Error!'), _('Debe ingresar el cliente!'))

        amount = order.amount_total - order.amount_paid

        data = self.read(cr, uid, ids, context=context)[0]
        fee = None
        is_credit_card = data.get('is_credit_card',False)

        if is_credit_card:
            fee_id = data.get('fee_id',False)
            
            card_number = data.get('card_number',False)
            auth_number = data.get('auth_number',False)
            if not fee_id :
                raise osv.except_osv(_('Error!'), _('Debe ingresar informacion del cupon/tarjeta!'))
        
        if data['fee_id']:
            fee = self.pool.get('sale.fee').browse(cr,uid,data['fee_id'][0])
            if fee and fee.coefficient > 0:
                tax_amount = 1

                if fee.product_id.taxes_id.amount:
                    tax_amount = 1 + fee.product_id.taxes_id.amount / 100


                vals_line = {
                    'product_id': fee.product_id.id,
                    'order_id': context['active_id'],
                    'display_name': fee.name,
                    'tax_ids':[(4,x.id) for x in fee.product_id.taxes_id ],
                    'qty': 1,
                    'price_unit': data['amount'] * fee.coefficient,
                    'price_subtotal': data['amount'] * fee.coefficient,
                    }
                
                self.pool.get('pos.order.line').create(cr,uid,vals_line)

                    
                #surcharge_amount = amount * fee.coefficient
                #tax_surcharge = surcharge_amount * fee.product_id.taxes_id.amount
                #total_amount = amount * ( 1 + fee.coefficient ) + tax_surcharge
                    
                vals = {
                    'amount': data['amount'] + data['amount'] * fee.coefficient
                    }
                #data['amount'] = amount * (1+fee.coefficient) + tax_surcharge
                
                self.pool.get('pos.make.payment').write(cr,uid,ids,vals)

        res = super(pos_make_payment,self).check(cr,uid,ids,context)
        if res == {'type' : 'ir.actions.act_window_close'}:
            if order.invoice_after_payment:
                #return order_obj.create_invoice_and_confirm(cr,uid,[order.id])
                order_obj.create_invoice_and_confirm(cr,uid,[order.id])
            #else:
                #return order_obj.print_receipt(cr,uid,[order.id])

        return res

class pos_order(osv.osv):
    _inherit = "pos.order"


    def add_payment(self, cr, uid, order_id, data, context=None):
        """Create a new payment for the order"""
        context = dict(context or {})
        statement_line_obj = self.pool.get('account.bank.statement.line')
        property_obj = self.pool.get('ir.property')
        order = self.browse(cr, uid, order_id, context=context)
        date = data.get('payment_date', time.strftime('%Y-%m-%d'))
        if len(date) > 10:
            timestamp = datetime.strptime(date, tools.DEFAULT_SERVER_DATETIME_FORMAT)
            ts = fields.datetime.context_timestamp(cr, uid, timestamp, context)
            date = ts.strftime(tools.DEFAULT_SERVER_DATE_FORMAT)
        args = {
            'amount': data['amount'],
            'date': date,
            'name': order.name + ': ' + (data.get('payment_name', '') or ''),
            'partner_id': order.partner_id and self.pool.get("res.partner")._find_accounting_partner(order.partner_id).id or False,
        }

        journal_id = data.get('journal', False)
        statement_id = data.get('statement_id', False)
        assert journal_id or statement_id, "No statement_id or journal_id passed to the method!"

        journal = self.pool['account.journal'].browse(cr, uid, journal_id, context=context)
        # use the company of the journal and not of the current user
        company_cxt = dict(context, force_company=journal.company_id.id)
        account_def = property_obj.get(cr, uid, 'property_account_receivable_id', 'res.partner', context=company_cxt)
        args['account_id'] = (order.partner_id and order.partner_id.property_account_receivable_id \
                             and order.partner_id.property_account_receivable_id.id) or (account_def and account_def.id) or False

        if not args['account_id']:
            if not args['partner_id']:
                msg = _('There is no receivable account defined to make payment.')
            else:
                msg = _('There is no receivable account defined to make payment for the partner: "%s" (id:%d).') % (order.partner_id.name, order.partner_id.id,)
            raise UserError(msg)

        context.pop('pos_session_id', False)

        for statement in order.session_id.statement_ids:
            if statement.id == statement_id:
                journal_id = statement.journal_id.id
                break
            elif statement.journal_id.id == journal_id:
                statement_id = statement.id
                break

        if not statement_id:
            raise UserError(_('You have to open at least one cashbox.'))
        journal_id = data.get('journal', False)

        fee_id = data.get('fee_id',False)
        if fee_id:
            fee_id = fee_id[0]
        auth_number = data.get('auth_number','')
        card_number = data.get('card_number','')

        args.update({
            'fee_id':fee_id,
            'auth_number':auth_number,
            'card_number':card_number,            
            'statement_id': statement_id,
            'pos_statement_id': order_id,
            'journal_id': journal_id,
            'ref': order.session_id.name,
        })

        statement_line_obj.create(cr, uid, args, context=context)

        return statement_id        

    def refund(self, cr, uid, ids, context=None):
        """Create a copy of order  for refund order"""
        clone_list = []
        line_obj = self.pool.get('pos.order.line')
        
        for order in self.browse(cr, uid, ids, context=context):
            current_session_ids = self.pool.get('pos.session').search(cr, uid, [
                ('state', '!=', 'closed'),
                ('user_id', '=', uid)], context=context)
            if not current_session_ids:
                raise UserError(_('To return product(s), you need to open a session that will be used to register the refund.'))

            clone_id = self.copy(cr, uid, order.id, {
                'name': order.name + ' REFUND', # not used, name forced by create
                'session_id': current_session_ids[0],
                'refund':True,
                'date_order': time.strftime('%Y-%m-%d %H:%M:%S'),
            }, context=context)
            clone_list.append(clone_id)

        for clone in self.browse(cr, uid, clone_list, context=context):
            for order_line in clone.lines:
                line_obj.write(cr, uid, [order_line.id], {
                    'qty': -order_line.qty
                }, context=context)

        abs = {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id':clone_list[0],
            'view_id': False,
            'context':context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
        return abs        