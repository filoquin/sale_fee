# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging
_logger = logging.getLogger(__name__)


class pos_session(models.Model):
    _inherit  = 'pos.session'

    coments = fields.Text(
        string='Comentarios',
    )

    @api.multi
    def automatic_close(self):
        self.ensure_one()
        self.balance_end_real = self.cash_register_balance_end
        for st in self.statement_ids:
            #if (st.journal_id.type not in ['bank', 'cash']):
                    #raise UserError(_("The type of the journal for your payment method should be bank or cash "))
            st.balance_end_real = st.balance_end
            st.button_confirm_bank()
        #self.cash_register_id.check_confirm_bank()

        self._confirm_orders()
        self.write({'state' : 'closed'})



        



class pos_make_refund(models.TransientModel):
    _name = "pos.make.refund"

    order_id = fields.Many2one(
         'pos.order',
         string='Order',
     ) 
    refund_menthod = fields.Selection(
        [('debit','add debit'),('cash','cash'),('same method','same method')],
            'Method'
        )



class pos_make_payment(models.TransientModel):
    _inherit = 'pos.make.payment'

    order_amount = fields.Float('Monto del pedido')
    auth_number = fields.Char('auth number')
    card_number = fields.Char('card number')    
    fee = fields.Integer('Fee')
    surcharge_amount = fields.Float('Monto Recargo')
    total_amount = fields.Float('total amount')

    fee_id = fields.Many2one('sale.fee',string='Plan de cuotas')    
    is_credit_card = fields.Boolean(string='Es tarjeta de crédito',related='journal_id.is_credit_card')
    is_debit_card = fields.Boolean(string='is debit card',related='journal_id.is_debit_card')

    amount_total = fields.Float('monto orden original') 
    partner_id = fields.Many2one('res.partner',string='Cliente')
    order_id = fields.Many2one('pos.order',string='Pedido')

    @api.one 
    @api.onchange('fee_id','amount')
    def change_fee_id(self):
        if self.fee_id:
            if self.fee_id.coefficient:
                tax_amount = 0
                if self.fee_id.product_id.taxes_id:

                    if len(self.fee_id.product_id.taxes_id) > 1:
                        raise ValidationError('El plan de cuotas tiene multiples impuestos configurados')
                    tax_amount = self.fee_id.product_id.taxes_id.amount

                self.fee = self.fee_id.fee
                self.surcharge_amount = self.amount * self.fee_id.coefficient * ( 1 + tax_amount/100)
                self.total_amount = self.amount + self.surcharge_amount
                """
                vals = {
                    'fee': self.fee,
                    'surcharge_amount': self.surcharge_amount,
                    'total_amount': self.total_amount,
                    }
                self.write(vals)
                """

            else:
                self.fee = 0
                self.surcharge_amount = 0
                self.total_amount = self.amount
        else:
            self.fee = 0
            self.surcharge_amount = 0
            self.total_amount = self.amount

class account_bank_statement_line(models.Model):
    _inherit = 'account.bank.statement.line'

    fee_id = fields.Many2one('sale.fee',string='Plan de cuotas')    
    auth_number = fields.Char('auth number')
    card_number = fields.Char('card number')
    

class pos_order(models.Model):
    _inherit = "pos.order"

    refund = fields.Boolean(
        string='refund',
    )
    refund_form_order_id = fields.Many2one(
        'pos.order',
        string='Refund form',
    )

    invoice_after_payment = fields.Boolean(
        string='invoice after payment',
    )

    @api.multi
    def close_wiz(self):
        return {'type': 'ir.actions.act_window_close'}

    @api.multi
    def print_receipt(self):
        action_dict =  self.env['report'].get_action(self,'point_of_sale.report_receipt')
        return action_dict

    @api.multi
    def create_invoice_and_confirm(self):
        self.ensure_one()
        rtn = self.action_invoice()

        if self.invoice_id:
            self.invoice_id.signal_workflow('invoice_open')
            action_dict =  self.env['report'].get_action(self.invoice_id,'account.report_invoice')
            #self.pool.get('pos.order')._match_payment_to_invoice(self._cr,self._uid,self,context=None)
            del action_dict['report_type']
            return rtn 

    '''
    def refund(self, cr, uid, ids, context=None):
        """Create a copy of order  for refund order"""
        clone_list = []
        line_obj = self.pool.get('pos.order.line')
        
        for order in self.browse(cr, uid, ids, context=context):
            current_session_ids = self.pool.get('pos.session').search(cr, uid, [
                ('state', '!=', 'closed'),
                ('user_id', '=', uid)], context=context)
            if not current_session_ids:
                raise UserError(_('To return product(s), you need to open a session that will be used to register the refund.'))

            clone_id = self.copy(cr, uid, order.id, {
                'name': order.name + ' REFUND', # not used, name forced by create
                'session_id': current_session_ids[0],
                'date_order': time.strftime('%Y-%m-%d %H:%M:%S'),
                'refund':True,
                'refund_form_order_id':order.id,
            }, context=context)

            clone_list.append(clone_id)

        for clone in self.browse(cr, uid, clone_list, context=context):
            for order_line in clone.lines:
                line_obj.write(cr, uid, [order_line.id], {
                    'qty': -order_line.qty
                }, context=context)

        abs = {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id':clone_list[0],
            'view_id': False,
            'context':context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }
        return abs'''
