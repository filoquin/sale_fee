# -*- coding: utf-8 -*-
from openerp import models, fields


class account_journal(models.Model):
    _inherit = 'account.journal'

    is_credit_card = fields.Boolean(string='is credit card')
    is_debit_card = fields.Boolean(string='is Debit card')
    debit_card_coefficient = fields.Float(string='Coeficiente')

    fee_ids = fields.One2many(
        'sale.fee',
        'journal_id',
        string='Field Label',
    )
