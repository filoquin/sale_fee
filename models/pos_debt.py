# -*- coding: utf-8 -*-
from openerp import models, fields, api
from openerp.exceptions import ValidationError
import logging
_logger = logging.getLogger(__name__)

class cancel_debt(models.TransientModel):
    _name = "cancel.debt"

    name = fields.Many2one('res.partner', 'Partner')    
    debt = fields.Float(
        string='debt',
        related="name.debt"
    )
    payment_amount = fields.Float(
        string='payment amount',
    ) 
    order_id = fields.Many2one(
        'pos.order',
        string='order',
    )

    @api.multi    
    def do_cancel_debt(self):
        session_id = self.env['pos.session'].search([('state','=','opened')], limit=1)
        if not session_id:
            #raise ValidationError('No hay sesion de caja')
            view = { 
            'name':"pos",
            'view_mode': 'form',
            'view_id': False,
            'view_type': 'form',
            'res_model': 'pos.session',
            'res_id': False,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'self',
            'domain': '',
                }
            return view

        if not len(self.order_id) :
            product = session_id.config_id.debt_dummy_product_id


            lines = []
            lines.append((0,0,{
                    'discount': 0,
                    'display_name': product.name,
                    'name': product.name,
                    'price_subtotal': 0,
                    'price_unit': 0,
                    'qty': 1,
                    'product_id': product.id,
                    'tax_ids':[(4,x.id) for x in product.taxes_id ],
                    }))

            pos_order = {
                'session_id': session_id.id,
                'name': '/',
                'partner_id': self.name.id,
                'sale_journal': session_id.config_id.journal_id.id,
                'location_id': session_id.config_id.stock_location_id.id,
                'user_id': self._uid,
                'pos_reference': '', 
                'lines':lines
                }

            order_id = self.env['pos.order'].create(pos_order)
            self.order_id = order_id.id 
            debt_journal = self.env['account.journal'].search([
                 ('company_id', '=', self.env.user.company_id.id), ('debt', '=', True)],limit=1)
            data = {
                'amount': -1* self.payment_amount,
                'payment_date': fields.Datetime.now(),
                'payment_name': debt_journal.name,
                'journal': debt_journal.id,
            }

            self.pool.get('pos.order').add_payment(self._cr, self._uid, order_id.id, data, context=None)

            """order_id.add_payment({
                'amount': -1* self.payment_amount,
                'payment_date': fields.Datetime.now(),
                'payment_name': debt_journal.name,
                'journal': debt_journal,
            })"""


            view = { 
                'name':"pos",
                'view_mode': 'form',
                'view_id': False,
                'view_type': 'form',
                'res_model': 'pos.make.payment',
                'res_id': False,
                'type': 'ir.actions.act_window',
                'nodestroy': True,
                'target': 'new',
                'context':{'pos_session_id' : session_id.id,'active_id':order_id.id},
                'domain': '',
            }
            return view
