# -*- coding: utf-8 -*-
from openerp import models, fields, api


class pos_transfer_cash(models.Model):
    _name = 'pos.transfer_cash'
    _description = 'transfer Cash'

    state = fields.Selection(
        [('draft','draft'),('transfer','transfer')],
        string='State',
        default = 'draft',
        required=True,
    )
    name = fields.Char(
        string='Ref',
        required=True,
    )
    orig_journal_id = fields.Many2one(
        'account.journal',
        string='Origin journal id',
        domain=[('type','=','cash'),],
        required=True,
    )

    dest_journal_id = fields.Many2one(
        'account.journal',
        string='Dest journal id',
        domain=[('type','=','cash'), ],
        required=True,
  

    )

    amount = fields.Float(
        string='amount',
        required=True,
  
    )


    @api.one
    def action_transfer(self):
        close_orig = False
        orig_st = self.env['account.bank.statement'].search(
            [('journal_id','=',self.orig_journal_id.id),('state','=','open')]
        )
        dest_st = self.env['account.bank.statement'].search(
            [('journal_id','=',self.dest_journal_id.id),('state','=','open')]
        )
        if len(dest_st)==0:
            raise('La caja de destino esta cerrada')

        if len(orig_st)==0:
            orig_st = self.env['account.bank.statement'].with_context(
                    {'journal_id':self.orig_journal_id.id}
                    ).create ({
                    'journal_id': self.orig_journal_id.id,
                    'user_id': self.env.user.id,
            })

            close_orig = True

        out_values = {
            'date': fields.Date.today(),
            'statement_id': orig_st.id,
            'journal_id': self.orig_journal_id.id,
            'amount': -self.amount or 0.0,
            'account_id': self.orig_journal_id.company_id.transfer_account_id.id,
            'ref': 'tc-%s' % (self.id),
            'name': self.name,
        }
        orig_st.write({'line_ids': [(0, False, out_values)]})
        in_values = {
            'date': fields.Date.today(),
            'statement_id': dest_st.id,
            'journal_id': dest_st.journal_id.id,
            'amount': self.amount or 0.0,
            'account_id': dest_st.journal_id.company_id.transfer_account_id.id,
            'ref': 'tc-%s' % (self.id),
            'name': self.name,
        }

        dest_st.write({'line_ids': [(0, False, in_values)]})
        
        if close_orig :
        	orig_st.balance_end_real = orig_st.balance_end
	        orig_st.button_confirm_bank()

        self.state='transfer'