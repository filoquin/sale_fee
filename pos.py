

class account_bank_statement_line(models.Model):
    _inherit = 'account.bank.statement.line'

    fee_id = fields.Many2one('sale.fee',string='Plan de cuotas')    
    nro_cupon = fields.Char('Nro cupon')
    nro_tarjeta = fields.Char('Nro tarjeta')
    installment_ids = fields.One2many(comodel_name='pos.order.installment',inverse_name='statement_line_id')    
    return_id = fields.Many2one('pos.return')


class pos_order_installment(models.Model):
    _name = 'pos.order.inrestallment'
    _description = 'Describe las cuotas asociadas a un pedido'

    @api.one
    def _compute_journal_id(self):
        if self.statement_line_id:
            self.journal_id = self.statement_line_id.journal_id.id

    order_id = fields.Many2one('pos.order',string='Pedido')
    statement_line_id = fields.Many2one('account.bank.statement.line',string='Medio de pago')
    journal_id = fields.Many2one('account.journal',string='Medio de Pago',compute='_compute_journal_id')
    nro_cuota = fields.Integer('Cuota')
    monto_capital = fields.Float('Monto Capital',digits_compute=dp.get_precision('Account'))
    monto_interes = fields.Float('Monto Interes',digits_compute=dp.get_precision('Account'))



class pos_config_journal(models.Model):
    _name = 'pos.config.journal'
    _description = 'Describe la relacion de medio de pago, journal, sesion'

    @api.one
    def _compute_next_printer_number(self):
        return_value = 0
        if self.journal_type == 'sale':    
            if self.journal_id.journal_class_id.document_class_id.name == 'A':
                return_value = self.config_id.journal_id.last_a_sale_document_completed + 1
            else:
                return_value = self.config_id.journal_id.last_b_sale_document_completed + 1
        self.next_printer_number = return_value

    @api.one
    def sync_numbers(self):
        if self.next_sequence_number != self.next_printer_number:
            vals = {
                'number_next_actual': self.next_printer_number 
                }
            sequence = self.journal_id.sequence_id
            sequence.write(vals)
    

    config_id = fields.Many2one('pos.config',string='Sesión',required=True)    
    responsability_id = fields.Many2one('afip.responsability',string='Responsabilidad AFIP',required=True)
    journal_id = fields.Many2one('account.journal',string='Diario',domain=[('type','in',['sale','sale_refund'])])
    journal_type = fields.Selection(selection=[('sale', 'Sale'),('sale_refund','Sale Refund'), ('purchase', 'Purchase'), ('purchase_refund','Purchase Refund'), ('cash', 'Cash'), ('bank', 'Bank and Checks'), ('general', 'General'), ('situation', 'Opening/Closing Situation')],related='journal_id.type')
    next_sequence_number = fields.Integer(string='Sig.Nro.Secuencia',related='journal_id.sequence_id.number_next_actual')
    next_printer_number = fields.Integer(string='Sig.Nro.Impresora',compute=_compute_next_printer_number)


class pos_config(models.Model):
    _inherit = 'pos.config'

    sale_journals = fields.One2many(comodel_name='pos.config.journal',inverse_name='config_id')
    point_of_sale = fields.Integer(string='Punto de Venta',required=True)
    bank_account = fields.Many2one('account.account',string='Cuenta Contable Bancos')
    cash_journal = fields.Many2one('account.journal',string='Metodo de Pago Caja',domain=[('type','=','cash')])

